package be.wantho.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Created by Wannes2 on 1/12/2017.
 */

public class SerializeUtils {
    public static void serializeToDiskUncompressed(Object o, File f) throws IOException {
        FileOutputStream fout = new FileOutputStream(f);
        ObjectOutputStream oout = new ObjectOutputStream(fout);

        oout.writeObject(o);

        oout.close();
        fout.close();
    }

    public static void serializeToDisk(Object o, File f) throws IOException {
        FileOutputStream fout = new FileOutputStream(f);
        DeflaterOutputStream dout = new DeflaterOutputStream(fout);
        ObjectOutputStream oout = new ObjectOutputStream(dout);

        oout.writeObject(o);

        oout.close();
        dout.close();
        fout.close();
    }

    public static Object deserializeFromDisk(File f) throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(f);
        InflaterInputStream iis = new InflaterInputStream(fis);
        ObjectInputStream ois = new ObjectInputStream(iis);

        Object res = ois.readObject();

        ois.close();
        iis.close();
        fis.close();

        return res;
    }
}
