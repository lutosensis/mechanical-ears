package be.wantho.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

import be.tarsos.transcoder.Attributes;
import be.tarsos.transcoder.Streamer;
import be.tarsos.transcoder.ffmpeg.EncoderException;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class AudioRead {

    private static float[] buf_to_float(short[] buffer){
        float scale = 1f / (float)(1 << ((8 * 2) - 1));
        float[] out = new float[buffer.length];

        for(int i = 0; i < buffer.length; i++){
            out[i] = ((float)buffer[i]) * scale;
        }

        return out;
    }

    private static float[] read_frame(AudioInputStream input) throws IOException {
        int frame_length = (int) input.getFrameLength();
        byte[] frame = new byte[frame_length];
        input.read(frame);

        ByteOrder byteorder = input.getFormat().isBigEndian() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN;
        ShortBuffer frame_shorts_buffer = ByteBuffer.wrap(frame).order(byteorder).asShortBuffer();
        short[] frame_shorts = new short[frame_shorts_buffer.capacity()];
        frame_shorts_buffer.get(frame_shorts);

        return buf_to_float(frame_shorts);
    }

//    public static float[] audio_read(AudioInputStream input_file) throws IOException {
//        float offset = 0;
//        Double duration = null;
//        List<float[]> y = new ArrayList<>();
//
//        float sr = input_file.getFormat().getSampleRate();
//        int channels = input_file.getFormat().getChannels();
//        int s_start = (int) (Math.floor(sr * offset) * channels);
//        int s_end = 0;
//
//        if(duration == null){
//            s_end = Integer.MAX_VALUE;
//        }else{
//            s_end = s_start + (int)(Math.ceil(sr * duration) * channels);
//        }
//
//        int num_read = 0;
//        while(input_file.available() > 0){
//            float[] frame = read_frame(input_file);
//            int num_read_prev = num_read;
//            num_read += frame.length;
//
//            if(num_read < s_start) continue;
//
//            if(s_end < num_read_prev) break;
//
//            if(s_end < num_read){
//                // crop the frame, end is reached
//                frame = Arrays.copyOfRange(frame, 0, s_end - num_read_prev);
//            }
//
//            if(num_read_prev <= s_start && s_start < num_read){
//                // beginning is in this frame
//                frame = Arrays.copyOfRange(frame, s_start - num_read_prev, frame.length);
//            }
//
//            y.add(frame);
//        }
//
//        if(y.size() == 0){
//            return new float[0];
//        }else{
//            int total_size = 0;
//            for (float[] frame : y) {
//                total_size += frame.length;
//            }
//
//            // concat all frames in one big array
//            float[] out = new float[total_size];
//            int out_start = 0;
//            for(int i = 0; i < y.size(); i++){
//                float[] frame = y.get(i);
//                System.arraycopy(frame, 0, out, out_start, frame.length);
//                out_start += frame.length;
//            }
//
//            return out;
//        }
//    }

    public static float[] audio_read(AudioInputStream inputAIS) throws IOException {
        AudioFormat audioFormat = inputAIS.getFormat();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // Read the audio data into a memory buffer.
        int nBufferSize = 1024 * audioFormat.getFrameSize();

        byte[] abBuffer = new byte[nBufferSize];
        while (true) {
            int nBytesRead = inputAIS.read(abBuffer);

            if (nBytesRead == -1) {
                break;
            }

            baos.write(abBuffer, 0, nBytesRead);
        }

        byte[] bytes = baos.toByteArray();

        ByteOrder byteorder = audioFormat.isBigEndian() ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN;
        ShortBuffer abAudioData = ByteBuffer.wrap(bytes).order(byteorder).asShortBuffer();
        short[] buffer = new short[abAudioData.capacity()];
        abAudioData.get(buffer);


        return buf_to_float(buffer);
    }

    public static float[] audio_read(Path filepath) throws EncoderException, IOException {
        return audio_read(filepath.toFile());
    }

    public static float[] audio_read(String filepath) throws EncoderException, IOException {
        return audio_read(new File(filepath));
    }

    public static float[] audio_read(File file) throws EncoderException, IOException {
        AudioInputStream ggstream = Streamer.stream(file.getPath(), new Attributes("s16le", 11025, 1));
        return audio_read(ggstream);
    }
}
