package be.wantho;

import com.almworks.sqlite4java.SQLiteException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import be.tarsos.transcoder.ffmpeg.EncoderException;
import be.wantho.data.Document;
import be.wantho.data.FingerPrintDatabase;
import be.wantho.data.FingerPrintMap;
import be.wantho.data.FingerPrintPartitionedMap;
import be.wantho.data.Match;
import be.wantho.io.AudioRead;
import be.wantho.io.SerializeUtils;

public class Main {

    public interface FileCallback {
        void result(Path file);
    }

    public static void loopDir(String path, final FileCallback callback) throws IOException {
        Files.walkFileTree(Paths.get(path), new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                callback.result(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                return FileVisitResult.TERMINATE;
            }
        });
    }

    public static void createSongsDb() throws IOException, SQLiteException {
        final FingerPrintMap memdb = new FingerPrintMap();

        loopDir("C:\\Users\\Wannes2\\Desktop\\Dropbox\\_Muziek", new FileCallback() {
            private int docid = 0;
            @Override
            public void result(Path file) {
                if(file.toString().endsWith(".mp3") || file.toString().endsWith(".m4a")){
                    System.out.println(file);

                    Analyzer a = new Analyzer();
                    try {
                        int[][] time_hashes = a.pcm2hashes(AudioRead.audio_read(file));
                        memdb.insertDocument(new Document(docid, file.toString()));
                        for (int[] time_hash : time_hashes) {
                            memdb.insert(docid, time_hash[0], time_hash[1]);
                        }
                    } catch (EncoderException | IOException e) {
                        e.printStackTrace();
                    }

                    docid++;
                }
            }
        });

        FingerPrintDatabase db2 = new FingerPrintDatabase("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\data\\testsongs3.db", memdb);
    }

    public static void testMemoryDb() throws IOException, EncoderException, SQLiteException {
//        FingerPrintDatabase db = new FingerPrintDatabase("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\data\\testsongs.db");

        final FingerPrintMap memdb = new FingerPrintMap();

        Analyzer asample = new Analyzer();
        int[][] hashes_sample = asample.pcm2hashes(AudioRead.audio_read("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\galwaygirl4s.wav"));

        Analyzer aref = new Analyzer();
        int[][] hashes_ref = aref.pcm2hashes(AudioRead.audio_read("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\galwaygirlsource.mp3"));

        memdb.insertDocument(new Document(1, "galway girl.mp3"));
        for (int[] aHashes_ref : hashes_ref) memdb.insert(1, aHashes_ref[0], aHashes_ref[1]);

        loopDir("C:\\Users\\Wannes2\\Desktop\\Dropbox\\_Muziek", new FileCallback() {
            int docid = 2;
            @Override
            public void result(Path file) {
                if(docid < 5 && file.toString().endsWith(".mp3")){
                    try {
                        Analyzer an = new Analyzer();
                        int[][] hashes = an.pcm2hashes(AudioRead.audio_read(file));

                        memdb.insertDocument(new Document(docid, file.toString()));

                        for (int[] hash : hashes) {
                            memdb.insert(docid, hash[0], hash[1]);
                        }

                        docid++;
                    } catch (EncoderException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        List<Match> matches = new Matcher(memdb).match(hashes_sample);

        for(int i = 0; i < Math.min(10, matches.size()); i++){
            Match match = matches.get(i);
            System.out.println(match.count + " " + match.document.id + " " + match.document.name);
        }

        System.out.println("sqlite version");

        FingerPrintDatabase db2 = new FingerPrintDatabase("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\data\\testsongs3.db", memdb);

        List<Match> matches2 = new Matcher(db2).match(hashes_sample);

        for(int i = 0; i < Math.min(10, matches2.size()); i++){
            Match match = matches2.get(i);
            System.out.println(match.count + " " + match.document.id + " " + match.document.name);
        }
    }

    public static void testPythonDb() throws IOException, EncoderException, SQLiteException {
        Analyzer asample = new Analyzer();
        int[][] hashes_sample = asample.pcm2hashes(AudioRead.audio_read("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\galwaygirl4s.wav"));

        FingerPrintDatabase db2 = new FingerPrintDatabase("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\data\\testsongs2.db");

        List<Match> matches2 = new Matcher(db2).match(hashes_sample);

        for(int i = 0; i < Math.min(10, matches2.size()); i++){
            Match match = matches2.get(i);
            System.out.println(match.count + " " + match.document.id + " " + match.document.name);
        }
    }

    public static void fingerprint_dir(String srcdir, final String destdir) throws IOException {
        final ExecutorService executor1 = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        loopDir(srcdir, new FileCallback() {
            @Override
            public void result(final Path file) {
                if(file.toString().endsWith(".mp3") || file.toString().endsWith(".m4a")){
                    if(file.toFile().length() == 0) return;
                    final File outfile = new File(destdir, file.getFileName().toString() + ".gz");
                    if(!outfile.exists()){
                            executor1.submit(new Callable<Object>() {
                                @Override
                                public Object call() throws Exception {
                                    try {
                                        Analyzer an = new Analyzer();
                                        int[][] hashes = an.pcm2hashes(AudioRead.audio_read(file));
                                        SerializeUtils.serializeToDisk(hashes, outfile);
                                    } catch (Error | EncoderException | IOException e) {
                                        e.printStackTrace();
                                    }
                                    return null;
                                }
                            });
                    }else{
                        System.out.println("skipping " + file.toString());
                    }
                }
            }
        });
    }

    public static void fingerprint_dir_to_db(String printdir, String dbfile) throws IOException, SQLiteException {
        final FingerPrintMap memdb = new FingerPrintMap();

        loopDir(printdir, new FileCallback() {
            private int docid = 0;
            @Override
            public void result(Path file) {
                if(file.toString().endsWith(".gz")){
                    if(docid % 100 == 0)
                        System.out.println(docid + " " + file);

                    try {
                        int[][] time_hashes = (int[][]) SerializeUtils.deserializeFromDisk(file.toFile());
                        memdb.insertDocument(new Document(docid, file.toString().substring(0, file.toString().length()-2)));
                        for (int[] time_hash : time_hashes) {
                            memdb.insert(docid, time_hash[0], time_hash[1]);
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    docid++;
                }
            }
        });

        FingerPrintPartitionedMap chunkeddb2 = new FingerPrintPartitionedMap(memdb, dbfile + "_16384", 16384);
//        FingerPrintDatabase db64 = new FingerPrintDatabase(dbfile + "_cz64.db", memdb, 64);
    }

    public static void main(String[] args) throws IOException, EncoderException, InterruptedException, SQLiteException {
//        testPythonDb();
//        fingerprint_dir("C:\\Users\\Wannes2\\Desktop\\Dropbox\\_Muziek", "C:\\Users\\Wannes2\\Desktop\\code\\_ir\\java_hashes");
//        fingerprint_dir("F:\\pixel_songs", "C:\\Users\\Wannes2\\Desktop\\code\\_ir\\java_hashes");

        fingerprint_dir_to_db("C:\\Users\\Wannes2\\Desktop\\code\\_ir\\java_hashes",
                "C:\\Users\\Wannes2\\Desktop\\code\\_ir\\data\\java_songs15k_chunks\\chunk");
    }
}
