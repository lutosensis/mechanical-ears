package be.wantho.data;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.DataFormatException;

import be.wantho.utils.CompressionUtils;
import be.wantho.utils.FingerprintPacker;

import static be.wantho.utils.Settings.BUCKET_SIZE;
import static be.wantho.utils.Settings.HASH_WIDTH;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class FingerPrintDatabase implements FingerPrintStore {

    private static final String TABLE_FINGERS = "fingers";
    private static final String TABLE_DOCUMENTS = "documents";
    private static final String INDEX_FINGERS = "sqlite_autoindex_fingers_1";
    private static final String FIELD_HASH = "hsh";
    private static final String FIELD_PACKS = "packs";
    private static final String FIELD_DOCID = "docid";
    private static final String FIELD_DOCNAME = "name";

    private final SQLiteConnection db;
    private final int chunksize;

    public FingerPrintDatabase(String dbfile, int chunksize) throws SQLiteException {
        this.chunksize = chunksize;

        db = new SQLiteConnection(new File(dbfile));
        db.open(false);
    }

    public FingerPrintDatabase(String dbfile, FingerPrintStore copyFrom, int chunksize) throws SQLiteException {
        this.chunksize = chunksize;

        File f = new File(dbfile);
        if (f.exists()) f.delete();

        db = new SQLiteConnection(f);
        db.open(true);

        createTables();

        Set<Integer> documents = new HashSet<>();

        // Insert all buckets
        db.exec("begin");
        SQLiteStatement st = db.prepare(String.format("INSERT INTO %s (%s, %s) VALUES (?, ?)", TABLE_FINGERS, FIELD_HASH, FIELD_PACKS));

        for (int hash = 0; hash < copyFrom.size()/chunksize; hash++) {
            byte[] bytes = new byte[BUCKET_SIZE * 2 * chunksize];
            boolean empty = true;
            for (int row = 0; row < chunksize; row++){
                char[] raw = copyFrom.getRaw(hash * chunksize + row);
                for (int i = 0; i < raw.length; i++) {
                    bytes[row * BUCKET_SIZE * 2 + i * 2] = (byte) (0xff & (raw[i] >> 8));
                    bytes[row * BUCKET_SIZE * 2 + i * 2 + 1] = (byte) (0xff & raw[i]);

                    if (raw[i] > 0){
                        empty = false;
                        int[] unpacked = FingerprintPacker.unpack(raw[i], i);
                        documents.add(unpacked[1]);
                    }
                }
            }

            try {
                if (!empty) {
                    st.bind(1, hash);
                    st.bind(2, CompressionUtils.compress(bytes));

                    st.step();
                    st.reset();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        db.exec("commit");
        st.dispose();

        // Insert all documents
        db.exec("begin");
        st = db.prepare(String.format("INSERT INTO %s (%s, %s) VALUES (?, ?)", TABLE_DOCUMENTS, FIELD_DOCID, FIELD_DOCNAME));

        for (Integer docid : documents) {
            Document document = copyFrom.getDocument(docid);
            if(document != null){
                st.bind(1, document.id);
                st.bind(2, sanitizeDocumentName(document.name));

                st.step();
                st.reset();
            }
        }

        db.exec("commit");
        st.dispose();
    }

    public FingerPrintDatabase(String dbfile, FingerPrintMap memdb) throws SQLiteException {
        this(dbfile, memdb, 1);
    }

    public FingerPrintDatabase(String s) throws SQLiteException {
        this(s, 1);
    }

    private void createTables() throws SQLiteException {
        db.exec("begin");
        db.exec(String.format("CREATE TABLE %s (%s INT PRIMARY KEY, %s BLOB)", TABLE_FINGERS, FIELD_HASH, FIELD_PACKS));
        db.exec(String.format("CREATE TABLE %s (%s INT PRIMARY KEY, %s TEXT)", TABLE_DOCUMENTS, FIELD_DOCID, FIELD_DOCNAME));
        db.exec("commit");
    }

    @Override
    public Document getDocument(int docid) {
        Document doc = new Document(docid, "");
        try {
            SQLiteStatement st = db.prepare(String.format("SELECT %s FROM %s WHERE %s = ?", FIELD_DOCNAME, TABLE_DOCUMENTS, FIELD_DOCID));
            st.bind(1, docid);
            if(st.step()){
                doc = new Document(docid, st.columnString(0));
            }
            st.dispose();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return doc;
    }

    @Override
    public int[][] get(int hash) {
        char[] packed_pairs = getRaw(hash);
        int[][] out = new int[packed_pairs.length][2];
        int count = 0;

        for (int i = 0; i < packed_pairs.length; i++) {
            if (packed_pairs[i] > 0) {
                out[count] = FingerprintPacker.unpack(packed_pairs[i], i);
                count++;
            }
        }

        return Arrays.copyOfRange(out, 0, count);//new int[count][2];
    }

    @Override
    public char[] getRaw(int hash) {
        try {
            SQLiteStatement st = db.prepare("SELECT " + FIELD_HASH + "," + FIELD_PACKS + " FROM " + TABLE_FINGERS + " WHERE " + FIELD_HASH + " = ?");
            st.bind(1, hash);

            if (st.step()) {
                CharBuffer buffer = ByteBuffer.wrap(CompressionUtils.decompress(st.columnBlob(1))).asCharBuffer();
                char[] result = new char[buffer.capacity()];
                buffer.get(result);
                return result;
            }

            st.dispose();
        } catch (SQLiteException | DataFormatException | IOException e) {
            e.printStackTrace();
        }
        return new char[0];
    }

    @Override
    public int size() {
        return 1 << HASH_WIDTH;
    }

    public static String sanitizeDocumentName(String name){
        name = name.replace("\\.mp3", "").replace("\\.m4a", "").replace("\\.gz", "");
        if(name.contains("\\")){
            name = name.substring(name.lastIndexOf("\\") + 1, name.length());
        }
        if(name.contains("/")){
            name = name.substring(name.lastIndexOf("/") + 1, name.length());
        }
        return name;
    }
}
