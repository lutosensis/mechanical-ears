package be.wantho.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import be.wantho.io.SerializeUtils;
import be.wantho.utils.FingerprintPacker;

import static be.wantho.utils.Settings.BUCKET_SIZE;
import static be.wantho.utils.Settings.DOC_ID_MAX_WIDTH;
import static be.wantho.utils.Settings.HASH_WIDTH;

/**
 * Created by Wannes2 on 4/12/2017.
 */

public class FingerPrintPartitionedMap implements FingerPrintStore {
    private final int chunks;
    private final int chunk_size;
    private final Map<Integer, Document> documents = new HashMap<>();

    public FingerPrintPartitionedMap(FingerPrintStore source, String filePath, int chunks) {
        Set<Integer> document_ids = new HashSet<>();

        this.chunks = chunks;
        this.chunk_size = (1 << HASH_WIDTH) / chunks;
        char[][] chunk = new char[chunk_size][BUCKET_SIZE];

        // Insert all buckets
        for (int hash = 0; hash < source.size(); hash++) {
            char[] raw = source.getRaw(hash);
            for (int i = 0; i < raw.length; i++) {
                if (raw[i] > 0){
                    int[] unpacked = FingerprintPacker.unpack(raw[i], i);
                    document_ids.add(unpacked[1]);
                }
            }

            chunk[hash % chunk_size] = raw;
            if(hash % chunk_size == chunk_size - 1){
                int chunk_index = hash / chunk_size;
                try {
                    SerializeUtils.serializeToDiskUncompressed(chunk, new File(filePath + "_" + chunk_index));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        for (Integer docid : document_ids) {
            Document document = source.getDocument(docid);
            if(document != null){
                documents.put(docid, document);
            }
        }

        try {
            SerializeUtils.serializeToDiskUncompressed(documents, new File(filePath + "_documents"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Document getDocument(int docid) {
        return null;
    }

    @Override
    public int[][] get(int hash) {
        return new int[0][];
    }

    @Override
    public char[] getRaw(int hash) {
        return new char[0];
    }

    @Override
    public int size() {
        return 0;
    }
}
