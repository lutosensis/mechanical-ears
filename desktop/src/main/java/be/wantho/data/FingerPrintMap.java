package be.wantho.data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import be.wantho.utils.FingerprintPacker;

import static be.wantho.utils.Settings.BUCKET_SIZE;
import static be.wantho.utils.Settings.BUCKET_SIZE_WIDTH;
import static be.wantho.utils.Settings.DOC_ID_MAX_WIDTH;
import static be.wantho.utils.Settings.HASH_WIDTH;

/**
 * Created by Wannes2 on 27/11/2017.
 */

public class FingerPrintMap implements FingerPrintStore {
    private final char[][] db = new char[1 << HASH_WIDTH][BUCKET_SIZE];
    private final int[] hash_counts = new int[1 << DOC_ID_MAX_WIDTH];
    private final Map<Integer, Document> documents = new HashMap<>();

    private int total_hashes, dropped_hashes, dropped_hashes_self;
    private Random rand = new Random(System.currentTimeMillis());

    public FingerPrintMap() {}

    public void insert(int docid, int time, int hash){
        if(!documents.containsKey(docid))
            throw new RuntimeException("trying to add fingerprint for unexisting document id " + docid);

        total_hashes++;

        int arr_pos = docid % BUCKET_SIZE;
        char time_docid_packed = FingerprintPacker.pack(time, docid);
        char curr_packing = db[hash][arr_pos];

        if(curr_packing == 0 || curr_packing == time_docid_packed){ // no collision
            db[hash][arr_pos] = time_docid_packed;
            hash_counts[docid]++;
        }else{
            dropped_hashes++;

            int[] unpacked = FingerprintPacker.unpack(curr_packing, arr_pos);
            int colliding_doc = unpacked[1];

            if(colliding_doc == docid) dropped_hashes_self++; // self collision

            int colliding_hash_count = hash_counts[colliding_doc];
            int self_hash_count = hash_counts[docid];

            int keep = rand.nextInt(self_hash_count + colliding_hash_count);
            if(keep > self_hash_count){ // replace the existing pair with new value
                db[hash][arr_pos] = time_docid_packed;
                hash_counts[colliding_doc]--;
                hash_counts[docid]++;
            }
        }
    }

    public void insertDocument(Document d){
        documents.put(d.id, d);
    }

    @Override
    public Document getDocument(int docid) {
        if(documents.containsKey(docid)) return documents.get(docid);
        return null;
    }

    /**
     * Get all time, docid pairs for a hash
     * @param hash Lookup key
     * @return An array of integer pairs (time, docid) for all entries in the hash's bucket
     */
    public int[][] get(int hash){
        char[] packed_pairs = db[hash];
        int[][] out = new int[packed_pairs.length][2];
        int count = 0;

        for(int i = 0; i < packed_pairs.length; i++){
            if(packed_pairs[i] > 0){
                out[count] = FingerprintPacker.unpack(packed_pairs[i], i);
                count++;
            }
        }

        return Arrays.copyOfRange(out, 0, count);//new int[count][2];
    }

    public char[] getRaw(int hash){
        return db[hash];
    }

    public int size() {
        return db.length;
    }
}
