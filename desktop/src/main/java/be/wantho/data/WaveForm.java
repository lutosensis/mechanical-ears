package be.wantho.data;

import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringBufferInputStream;

import jdk.nashorn.internal.parser.JSONParser;
import jdk.nashorn.internal.runtime.JSONFunctions;

/**
 * Created by Wannes2 on 24/11/2017.
 */

public class WaveForm {
    public static final double[]   WAVEFORM;
    public static final double[][] YFRAMES;

    public static String readFileAsString(String path) throws IOException {
        File f = new File(path);
        FileInputStream fis = new FileInputStream(f);

        byte[] data = new byte[1024];
        String result = "";

        while(fis.available() > 0){
            int size = fis.read(data);
            result += new String(data, 0, size);
        }

        return result;
    }

    static {
        double[] waveform = new double[0];

        try {
            String result = readFileAsString("C:\\Users\\Wannes2\\AndroidStudioProjects\\MechanicalEars\\libears\\src\\main\\resources\\wave.txt");
            String[] numbers = result.split(",");
            waveform = new double[numbers.length];
            for(int i = 0; i < waveform.length; i++) waveform[i] = Double.parseDouble(numbers[i]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        WAVEFORM = waveform;

        double[][] yframes;

        try {
            yframes = new Gson().fromJson(readFileAsString("C:\\Users\\Wannes2\\AndroidStudioProjects\\MechanicalEars\\libears\\src\\main\\resources\\y_frames.json"), double[][].class);
        } catch (IOException e) {
            e.printStackTrace();
            yframes = new double[0][0];
        }

        YFRAMES = yframes;
    }
}