package be.wantho.mechanicalears;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class RecordingThread extends Thread {

    public interface RecordingCallback {
        boolean onFrame(float[] frame);
    }

    private static class Work implements Runnable {

        private static final int BUFFER_SIZE = 1024;

        private final RecordingCallback callback;
        private final AudioRecord recorder;

        public Work(RecordingCallback callback) {
            this.callback = callback;
            this.recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 11025,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE);
        }

        private static float[] buf_to_float(short[] buffer){
            float scale = 1f / (float)(1 << ((8 * 2) - 1));
            float[] out = new float[buffer.length];

            for(int i = 0; i < buffer.length; i++){
                out[i] = ((float)buffer[i]) * scale;
            }

            return out;
        }

        @Override
        public void run() {
            Log.d(Constants.TAG, "starting recording");

            recorder.startRecording();

            short[] buffer = new short[BUFFER_SIZE];

            while(true){
                int read = recorder.read(buffer, 0, buffer.length);

//                Log.d(Constants.TAG, "read " + read + " bytes from mic");

                if(!callback.onFrame(buf_to_float(Arrays.copyOfRange(buffer, 0, read))))
                    break;
            }

            recorder.stop();
            recorder.release();
        }
    }

    public RecordingThread(RecordingCallback callback) {
        super(new Work(callback));
    }
}
