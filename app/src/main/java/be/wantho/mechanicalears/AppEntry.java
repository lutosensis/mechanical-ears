package be.wantho.mechanicalears;

import android.app.Application;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wannes2 on 29/11/2017.
 */

public class AppEntry extends Application {

    private boolean running = false;
    private Handler mainThread;
    private Thread taskThread;
    private List<Task<Object>> taskQueue = new ArrayList<>();
    private List<Task<Object>> canceledTaskQueue = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        running = true;

        mainThread = new Handler(getMainLooper());
        taskThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(running){
                    if(taskQueue.size() > 0){
                        final Task<Object> t = taskQueue.get(0);
                        taskQueue.remove(0);

                        if(canceledTaskQueue.contains(t)){
                            canceledTaskQueue.remove(t);
                            continue;
                        }

                        final Object result = t.run();
                        mainThread.post(new Runnable() {
                            @Override
                            public void run() {
                                if(!canceledTaskQueue.contains(t)){
                                    if(running)
                                        t.ui(result);
                                }else{
                                    canceledTaskQueue.remove(t);
                                }
                            }
                        });
                    }else{
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        taskThread.start();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        running = false;
    }

    public <T> void queueTask(Task<T> t){
        taskQueue.add((Task<Object>) t);
    }

    public <T> void cancelTask(Task<T> t){
        canceledTaskQueue.add((Task<Object>) t);
    }

    public interface Task<T> {
        T run();
        void ui(T data);
    }
}
