package be.wantho.mechanicalears;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import be.wantho.data.Document;
import be.wantho.data.Match;

public class ResultActivity extends AppCompatActivity {

    private static final String ARG_DATA = "ARG_DATA";

    public static Intent create(Context c, List<Match> matches){
        Intent i = new Intent(c, ResultActivity.class);
        i.putExtra(ARG_DATA, new Gson().toJson(matches));
        return i;
    }

    private TextView bestMatch;
    private TextView otherMatches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        bestMatch = (TextView) findViewById(R.id.best_match);
        otherMatches = (TextView) findViewById(R.id.matches);

        if(getIntent().hasExtra(ARG_DATA)){
            List<Match> matches = new Gson().fromJson(getIntent().getStringExtra(ARG_DATA), new TypeToken<List<Match>>(){}.getType());

            Match best = matches.get(0);
            bestMatch.setText(formatDocumentName(best.document) + "\n" + best.count + " matches");

            String data = "";
            for (int i = 1; i < matches.size(); i++) {
                data += matches.get(i).count + " " + formatDocumentName(matches.get(i).document) + "\n";
            }
            otherMatches.setText(data);
        }
    }

    private static String formatDocumentName(Document doc){
        String name = doc.name;
        if(name.contains(".")){
            name = name.substring(0, name.lastIndexOf("."));
        }
        if(name.contains("\\")){
            name = name.substring(name.lastIndexOf("\\") + 1, name.length());
        }
        return name;
    }
}
