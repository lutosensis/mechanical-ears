package be.wantho.mechanicalears.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.ArrayList;
import java.util.List;

import be.wantho.mechanicalears.Constants;

import static be.wantho.mechanicalears.Constants.MAX_BUFFER_SIZE;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class EqualizerView extends View {

    public enum State {
        PAUSED,
        LISTENING,
        CALCULATING
    }

    private Path path = new Path();
    private Matrix m = new Matrix();
    private Drawable micIconActive = MaterialDrawableBuilder.with(getContext())
                                        .setIcon(MaterialDrawableBuilder.IconValue.MICROPHONE)
                                        .setColor(Color.WHITE)
                                        .build();
    private Drawable micIconInactive = MaterialDrawableBuilder.with(getContext())
                                        .setIcon(MaterialDrawableBuilder.IconValue.MICROPHONE)
                                        .setColor(Constants.COLOR_ACCENT)
                                        .build();

    public interface Initializer<T> {
        void init(T data);
    }

    public static <T> T init(T data, Initializer<T> apply){
        apply.init(data);
        return data;
    }

    private State state = State.PAUSED;

    private double amplitude = 0;
    private double innerRadiusScale = 0;
    private double outerRadiusScale = 1;

    private long calcStartTime = 0;

    private static final int RECTANGLE_COUNT = 360 * 2;
    private static final int GROUP_FACTOR = MAX_BUFFER_SIZE / RECTANGLE_COUNT;
    private List<Double> waveFormBuffer = new ArrayList<>();
    private List<Double> waveForm = new ArrayList<>();

    private Paint p = init(new Paint(Paint.ANTI_ALIAS_FLAG), new Initializer<Paint>() {
        @Override
        public void init(Paint data) {
            data.setColor(Constants.COLOR_ACCENT);
        }
    });
    private Paint whitep = init(new Paint(Paint.ANTI_ALIAS_FLAG), new Initializer<Paint>() {
        @Override
        public void init(Paint data) {
            data.setColor(Color.WHITE);
        }
    });
    private GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
            new int[]{Constants.COLOR_ACCENT, Color.BLUE});
    private GradientDrawable circleGradient = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
            new int[]{Constants.COLOR_ACCENT, Constants.COLOR_ACCENT, Color.BLUE});

    public EqualizerView(Context context) {
        super(context);
    }

    public EqualizerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EqualizerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private double scaleAmplitude(double ampl){
        double scaled = Math.sqrt(Math.abs(ampl));
        // return a value in [-1,1]
        return ampl > 0 ? scaled : -scaled;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        double scaled = (scaleAmplitude(amplitude) + 1) / 2;
        scaled = .51;

        int r = Math.min(canvas.getWidth(), canvas.getHeight()) / 2;
        int c_x = canvas.getWidth() / 2, c_y = canvas.getHeight() / 2;

        if(state == State.LISTENING || state == State.CALCULATING){
            canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, (float) (scaled * r), p);

            int waveHeight = canvas.getHeight() / 2;
            int waveWidth = canvas.getWidth();

            while(waveForm.size() > RECTANGLE_COUNT){
                waveForm.remove(0);
            }

            int indexOffset = Math.max(0, waveForm.size() - RECTANGLE_COUNT);

            path.reset();
            path.moveTo(canvas.getWidth() / 2, canvas.getHeight() / 2);

            canvas.save();

            int endIndex = Math.min(waveForm.size(), RECTANGLE_COUNT);
            for(int i = endIndex-1; i >= 0; i--){
                float ampl = (float) scaleAmplitude(waveForm.get(indexOffset + i).floatValue());

//            canvas.drawRect(waveWidth / 2 - 1f, waveHeight/2 - Math.abs(waveHeight*ampl), waveWidth / 2 + 1f, waveHeight/2, p);
//            canvas.rotate(360f / waveWidth, canvas.getWidth() / 2, canvas.getHeight() / 2);
//            path.addRect(waveWidth / 2 - 1f, waveHeight/2 - Math.abs(waveHeight*ampl), waveWidth / 2 + 1f, waveHeight/2, Path.Direction.CW);
                path.lineTo(c_x, c_y - r/2 - Math.abs(r/2*ampl));
                m.reset();
                m.setRotate(360f / RECTANGLE_COUNT, canvas.getWidth() / 2, canvas.getHeight() / 2);
                path.transform(m);

//            path.addRect(i, waveY*1.5f - Math.abs(waveHeight/2*ampl), i+1, waveY*1.5f, Path.Direction.CW);
//            path.addRect(i, waveY*1.5f, i+1, waveY*1.5f + Math.abs(waveHeight/2*ampl), Path.Direction.CW);
            }

            path.close();

            canvas.restore();

            canvas.save();

            canvas.clipPath(path);

//        gradient.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
//        gradient.setBounds(0, (int) (waveY*1.5), waveWidth, (int) (waveY*1.5 + waveHeight/2));
//        gradient.draw(canvas);
//        gradient.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
//        gradient.setBounds(0, (int) (waveY*1.5 - waveHeight/2), waveWidth, (int) (waveY*1.5));
//        gradient.draw(canvas);

            circleGradient.setGradientType(GradientDrawable.RADIAL_GRADIENT);
            circleGradient.setGradientCenter(.5f, .5f);
            circleGradient.setGradientRadius(canvas.getHeight()/2);
            circleGradient.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            circleGradient.draw(canvas);

            canvas.restore();

            if(state == State.CALCULATING){
                int dotcount = 4;
                int dotr = (int) (r*.05f);
                int dotw = dotr * 2;

                int dotSpacing = (r - (dotw * dotcount)) / (dotcount + 1);

                float animatedfraction = (float)(System.currentTimeMillis() % 1500);

                for(int i = 0; i < dotcount; i++){
                    int x_pos = (dotSpacing + dotw) * (i+1) - dotr;

                    float dot_scale = 1;
                    float adjusted_fraction = (float) (Math.min(300, Math.max(0, animatedfraction - i*150)) / 300f * Math.PI);
                    dot_scale += Math.sin(adjusted_fraction) * .75f;

                    whitep.setColor(Color.WHITE);
                    canvas.drawCircle(c_x - r/2 + x_pos, c_y, r*.05f*dot_scale, whitep);
                }
            }
        }

        int icon_size = (int) (r*.4f);
        if(state == State.PAUSED){
            micIconInactive.setBounds(c_x - icon_size, c_y - icon_size, c_x + icon_size, c_y + icon_size);
            micIconInactive.draw(canvas);
        }else if(state == State.LISTENING){
            micIconActive.setBounds(c_x - icon_size, c_y - icon_size, c_x + icon_size, c_y + icon_size);
            micIconActive.draw(canvas);
        }

        if(state == State.CALCULATING){
            postInvalidate();
        }
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = (amplitude + this.amplitude) / 2;
        postInvalidate();
    }

    public void expandWaveForm(double data){
        waveFormBuffer.add(data);
        if(waveFormBuffer.size() >= GROUP_FACTOR){
            double sum = 0;
            for (Double val : waveFormBuffer) sum += val;

            waveForm.add(sum / waveFormBuffer.size());
            waveFormBuffer.clear();

            postInvalidate();
        }
    }

    public void clearWaveForm(){
        waveForm.clear();
        waveFormBuffer.clear();
        postInvalidate();
    }

    public void setColor(int color){
        p.setColor(color);
        postInvalidate();
    }

    public void setInnerRadiusScale(double innerRadiusScale) {
        this.innerRadiusScale = innerRadiusScale;
        postInvalidate();
    }

    public void setOuterRadiusScale(double outerRadiusScale) {
        this.outerRadiusScale = outerRadiusScale;
        postInvalidate();
    }

    public void setState(State state) {
        this.state = state;

        if(state == State.CALCULATING){
            calcStartTime = System.currentTimeMillis();
        }

        postInvalidate();
    }

    public State getState() {
        return state;
    }
}
