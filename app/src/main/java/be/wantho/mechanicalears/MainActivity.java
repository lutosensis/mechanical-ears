package be.wantho.mechanicalears;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import be.wantho.Analyzer;
import be.wantho.Matcher;
import be.wantho.data.Match;
import be.wantho.mechanicalears.data.FingerPrintDatabase;
import be.wantho.mechanicalears.data.FingerPrintPartitionedMap;
import be.wantho.mechanicalears.view.EqualizerView;

import static be.wantho.mechanicalears.Constants.SAMPLE_COUNT;
import static be.wantho.mechanicalears.Constants.SAMPLE_SIZE_K;

public class MainActivity extends AppCompatActivity implements MatchCallback {

    private static final int PERMISSION_REQUEST_RECORD_AUDIO = 11;

    private FingerPrintPartitionedMap db;
    private Matcher matcher;

    private EqualizerView equalizer;

    private boolean recording = false;
    private List<AppEntry.Task> matchTasks = new ArrayList<>();
    private AppEntry app;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        try {
//            db = new FingerPrintDatabase(MainActivity.this, getResources().openRawResource(R.raw.java_songs11k_cz64), 64);
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Couldn't create database", Toast.LENGTH_SHORT).show();
//            finish();
//            return;
//        }

        try {
            List<Integer> ids = new ArrayList<>();
            for(int i = 0; i < 16384; i++){
                ids.add(getResources().getIdentifier("chunk_16384_" + i, "raw", getPackageName()));
            }
            db = new FingerPrintPartitionedMap(this, ids, R.raw.chunk_16384_documents, new FingerPrintPartitionedMap.StateListener() {
                @Override
                public void onStart() {
                    dialog = ProgressDialog.show(MainActivity.this, "", "Loading Song Database...");
                }

                @Override
                public void onFinished() {
                    dialog.dismiss();
                }

                @Override
                public void onError() {
                    dialog.dismiss();
                    Toast.makeText(MainActivity.this, "Could not load song database, please restart the application", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "error creating database", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        matcher = new Matcher(db);

        app = (AppEntry) getApplication();
        equalizer = (EqualizerView) findViewById(R.id.equalizer);

        equalizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkMicrophonePermission()){
                    if(!recording && equalizer.getState() == EqualizerView.State.PAUSED){
                        startRecording();
                    }else{
                        stopRecording();
                    }
                }
            }
        });
//
//        float[] wave = getWavesample();
//
//        System.out.println(wave.length);
//
//        Analyzer an = Analyzer.Builder.with().shifts(4).build();
//        int[][] hashes = an.pcm2hashes(wave);
//
//        Analyzer an2 = Analyzer.Builder.with().shifts(4).build();
//        int[][] hashes2 = an2.pcm2hashes(Arrays.copyOfRange(wave, 0, 10240));
//
//        Analyzer an3 = Analyzer.Builder.with().shifts(4).build();
//        int[][] hashes3 = an3.pcm2hashes(Arrays.copyOfRange(wave, 10240, 10240*2));
//
//        for (int i = 0; i < hashes2.length; i++) {
//            boolean eq = hashes[i][0] == hashes2[i][0] && hashes[i][1] == hashes2[i][1];
//            System.out.println(hashes[i][0] + "\t" + hashes[i][1] + "\t-\t" + hashes2[i][0] + "\t" + hashes2[i][1] + "\t" + eq);
//        }
//        for (int i = 0; i < hashes3.length; i++) {
//            int j = i + hashes2.length;
//            boolean eq = hashes[j][0] == hashes3[i][0] && hashes[j][1] == hashes3[i][1];
//            System.out.println(hashes[j][0] + "\t" + hashes[j][1] + "\t-\t" + hashes3[i][0] + "\t" + hashes3[i][1] + "\t" + eq);
//        }
    }

    private boolean checkMicrophonePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                new AlertDialog.Builder(this)
                        .setMessage("Mechanical Ears needs access to your microphone in order to record a short audio fragment.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.RECORD_AUDIO},
                                        PERMISSION_REQUEST_RECORD_AUDIO);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                .show();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSION_REQUEST_RECORD_AUDIO);
            }

            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    equalizer.performClick();
                }
                break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRecording();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        db.close();
    }

    private void startRecording(){
        equalizer.setState(EqualizerView.State.LISTENING);
        equalizer.clearWaveForm();

        recording = true;
        RecordingThread rt = new RecordingThread(new RecordingThread.RecordingCallback() {

            float[] recorded = new float[1024 * SAMPLE_SIZE_K * SAMPLE_COUNT];
            int offset = 0;

            @Override
            public boolean onFrame(float[] frame) {
                System.arraycopy(frame, 0, recorded, offset, Math.min(frame.length, recorded.length - offset));
                offset += frame.length;
                if(offset >= recorded.length){
                    Log.d(Constants.TAG, "done recording, calculating matches...");

                    startCalculting();

                    match(recorded, MainActivity.this, true);
                }else if((offset / 1024) % SAMPLE_SIZE_K == 0){
                    match(Arrays.copyOfRange(recorded, 0, offset), MainActivity.this, false);
                }

                visualizeFrame(frame);

                return recording;
            }
        });
        rt.start();
    }

    private void startCalculting() {
        equalizer.setState(EqualizerView.State.CALCULATING);
        recording = false;
    }

    private void stopRecording(){
        equalizer.setState(EqualizerView.State.PAUSED);
        recording = false;

        for (AppEntry.Task task : matchTasks)
            app.cancelTask(task);
        matchTasks.clear();
    }

    private void visualizeFrame(float[] frame) {
        for (float v : frame) {
            equalizer.expandWaveForm(v);
        }
        visualizeFrame(frame, 0, 184);
    }

    private void visualizeFrame(final float[] frame, final int index, final int offset){
        equalizer.setAmplitude(frame[index]);

        final int newindex = index + offset;
        if(newindex < frame.length){
            equalizer.postDelayed(new Runnable() {
                @Override
                public void run() {
                    visualizeFrame(frame, newindex, offset);
                }
            }, 16);
        }
    }

    private void match(final float[] pcmdata, final MatchCallback callback, final boolean isfinal){
        AppEntry.Task<List<Match>> t = new AppEntry.Task<List<Match>>() {
            @Override
            public List<Match> run() {
                Analyzer an = Analyzer.Builder.with().shifts(4).build();
                int[][] hashes = an.pcm2hashes(pcmdata);

                Log.d(Constants.TAG, "computing matches");
                return matcher.match(hashes);
            }

            @Override
            public void ui(List<Match> data) {
                boolean lastmatch = false;

                if(data.size() > 5){
                    int median = data.get(data.size() / 2).count;
                    int thresh = median * 5; // five times more matches than the median amount as threshold

                    List<Match> filtered = new ArrayList<>();
                    for (Match match : data) {
                        if(match.count > thresh){
                            filtered.add(match);
                        }
                    }

                    if(filtered.size() == 1){
                        stopRecording();
                        lastmatch = true;
                    }
                }

                if (isfinal){
                    stopRecording();
                    lastmatch = true;
                }

                callback.result(data, lastmatch);
            }
        };
        matchTasks.add(t);
        ((AppEntry)getApplication()).queueTask(t);
    }

    @Override
    public void result(List<Match> matches, boolean isfinal) {
        if(isfinal)
            startActivity(ResultActivity.create(this, matches.subList(0, Math.min(10, matches.size()))));
    }

    private float[] getWavesample(){
//        InputStream result = getResources().openRawResource(R.raw.wave);
        InputStream result = null;
        byte[] buffer = new byte[1024];
        int end;
        try {
            String resultstring = "";
            while((end = result.read(buffer)) > 0){
                resultstring += new String(buffer, 0, end);
            }
            String[] numbers = resultstring.split(",");
            float[] waveform = new float[numbers.length];
            for(int i = 0; i < waveform.length; i++) waveform[i] = Float.parseFloat(numbers[i]);

            return waveform;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new float[0];
    }
}
