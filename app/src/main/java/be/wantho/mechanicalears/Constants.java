package be.wantho.mechanicalears;

import android.graphics.Color;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class Constants {
    public static final String TAG = "MechanicalEars";
    public static final int SAMPLE_SIZE_K = 64;
    public static final int SAMPLE_COUNT = 2;
    public static final int BUFFER_SIZE = 1024;
    public static final int MAX_BUFFER_SIZE = SAMPLE_SIZE_K * SAMPLE_COUNT * BUFFER_SIZE;
    public static final int COLOR_ACCENT = Color.parseColor("#e21a34");
}
