package be.wantho.mechanicalears.data;

import android.content.Context;
import android.support.annotation.RawRes;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.InflaterInputStream;

import be.wantho.data.Document;
import be.wantho.data.FingerPrintStore;
import be.wantho.mechanicalears.AppEntry;
import be.wantho.utils.FingerprintPacker;

import static be.wantho.utils.Settings.HASH_WIDTH;

/**
 * Created by Wannes2 on 4/12/2017.
 */

public class FingerPrintPartitionedMap implements FingerPrintStore {
    private final Context context;
    private final List<Integer> chunkIds;
    private final int chunkSize;
    private final StateListener callback;
    private Map<Integer, Document> documents;
    private int cachedChunk = -1;
    private char[][] chunk = null;

    public FingerPrintPartitionedMap(final Context c, List<Integer> chunksRes, @RawRes final int documentsRes, final StateListener callback) throws IOException, ClassNotFoundException {
        context = c;
        this.callback = callback;
        chunkIds = chunksRes;
        chunkSize = size() / chunkIds.size();

        callback.onStart();

        ((AppEntry)c.getApplicationContext()).queueTask(new AppEntry.Task<Map<Integer, Document>>() {
            @Override
            public Map<Integer, Document> run() {
                try {
                    return (Map<Integer, Document>) deserializeRawUncompressed(c.getResources().openRawResource(documentsRes));
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            public void ui(Map<Integer, Document> data) {
                if(data != null){
                    documents = data;
                    callback.onFinished();
                }else{
                    callback.onError();
                }
            }
        });
    }

    @Override
    public Document getDocument(int docid) {
        if(documents.containsKey(docid)){
            return documents.get(docid);
        }
        return new Document(docid, "");
    }

    @Override
    public int[][] get(int hash) {
        char[] packed_pairs = getRaw(hash);
        int[][] out = new int[packed_pairs.length][2];
        int count = 0;

        for (int i = 0; i < packed_pairs.length; i++) {
            if (packed_pairs[i] > 0) {
                out[count] = FingerprintPacker.unpack(packed_pairs[i], i);
                count++;
            }
        }

        return Arrays.copyOfRange(out, 0, count);//new int[count][2];
    }

    @Override
    public char[] getRaw(int hash) {
        int chunk_index = hash / chunkSize;
        if(chunk != null && chunk_index == cachedChunk){
            return chunk[hash % chunkSize];
        }else{
            try {
                chunk = (char[][]) deserializeRawUncompressed(context.getResources().openRawResource(chunkIds.get(chunk_index)));
                cachedChunk = chunk_index;
                return chunk[hash % chunkSize];
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return new char[0];
    }

    @Override
    public int size() {
        return 1 << HASH_WIDTH;
    }

    public static Object deserializeRaw(InputStream is) throws IOException, ClassNotFoundException {
        InflaterInputStream iis = new InflaterInputStream(is);
        ObjectInputStream ois = new ObjectInputStream(iis);

        Object res = ois.readObject();

        ois.close();
        iis.close();
        is.close();

        return res;
    }

    public static Object deserializeRawUncompressed(InputStream is) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(is);

        Object res = ois.readObject();

        ois.close();
        is.close();

        return res;
    }

    public interface StateListener {
        void onStart();
        void onFinished();
        void onError();
    }
}
