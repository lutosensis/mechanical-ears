package be.wantho.mechanicalears.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.zip.DataFormatException;

import be.wantho.data.Document;
import be.wantho.data.FingerPrintStore;
import be.wantho.utils.CompressionUtils;
import be.wantho.utils.FingerprintPacker;

import static be.wantho.mechanicalears.Constants.BUFFER_SIZE;
import static be.wantho.utils.Settings.BUCKET_SIZE;
import static be.wantho.utils.Settings.HASH_WIDTH;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class FingerPrintDatabase implements FingerPrintStore {

    private static final String TABLE_FINGERS = "fingers";
    private static final String TABLE_DOCUMENTS = "documents";
    private static final String INDEX_FINGERS = "sqlite_autoindex_fingers_1";
    private static final String FIELD_HASH = "hsh";
    private static final String FIELD_PACKS = "packs";
    private static final String FIELD_DOCID = "docid";
    private static final String FIELD_DOCNAME = "name";

    private final SQLiteDatabase db;
    private final int chunksize;

    public FingerPrintDatabase(Context c, InputStream source, int chunksize) throws IOException {
        this.chunksize = chunksize;

        File dbfile = c.getDatabasePath("prints.db");
        // copy the database from source if it doesn't exist
        if(!dbfile.exists()){
            if(!dbfile.getParentFile().exists()) dbfile.getParentFile().mkdir();

            OutputStream outputStream = new FileOutputStream(dbfile);
            byte[] buffer = new byte[1024];

            int numOfBytesToRead;
            while((numOfBytesToRead = source.read(buffer)) > 0)
                outputStream.write(buffer, 0, numOfBytesToRead);

            source.close();
            outputStream.close();
        }
        db = SQLiteDatabase.openDatabase(dbfile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public Document getDocument(int docid) {
        Document doc = new Document(docid, "");

        Cursor result = db.rawQuery(String.format("SELECT %s FROM %s WHERE %s = ?", FIELD_DOCNAME, TABLE_DOCUMENTS, FIELD_DOCID), new String[]{"" + docid});
        if(result.getCount() > 0 && result.moveToFirst()){
            doc = new Document(docid, result.getString(0));
        }
        result.close();

        return doc;
    }

    @Override
    public int[][] get(int hash) {
        char[] packed_pairs = getRaw(hash);
        int[][] out = new int[packed_pairs.length][2];
        int count = 0;

        for (int i = 0; i < packed_pairs.length; i++) {
            if (packed_pairs[i] > 0) {
                out[count] = FingerprintPacker.unpack(packed_pairs[i], i);
                count++;
            }
        }

        return Arrays.copyOfRange(out, 0, count);//new int[count][2];
    }

    @Override
    public char[] getRaw(int hash) {
        char[] raw_result = new char[0];
        int db_index = hash / chunksize;
        int db_offset = hash % chunksize;

        try {
            Cursor result = db.rawQuery("SELECT " + FIELD_HASH + "," + FIELD_PACKS + " FROM " + TABLE_FINGERS + " WHERE " + FIELD_HASH + " = ?", new String[]{""+db_index});

            if (result.getCount() > 0 && result.moveToFirst()) {
                CharBuffer buffer = ByteBuffer.wrap(CompressionUtils.decompress(result.getBlob(1))).asCharBuffer();

                raw_result = new char[BUCKET_SIZE];
                for(int i = 0; i < db_offset+1; i++)
                    buffer.get(raw_result);
            }

            result.close();
        } catch (DataFormatException | IOException e) {
            e.printStackTrace();
        }

        return raw_result;
    }

    @Override
    public int size() {
        return 1 << HASH_WIDTH;
    }

    public void close() {
        db.close();
    }
}
