package be.wantho.mechanicalears;

import java.util.List;

import be.wantho.data.Match;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public interface MatchCallback {
    void result(List<Match> matches, boolean isfinal);
}
