package be.wantho.data;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class Match {
    public final Document document;
    public final int count;

    public Match(Document document, int count) {
        this.document = document;
        this.count = count;
    }
}
