package be.wantho.data;

import java.io.Serializable;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class Document implements Serializable {
    public final int id;
    public final String name;

    public Document(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
