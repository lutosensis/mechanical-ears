package be.wantho.data;

/**
 * Created by Wannes2 on 1/12/2017.
 */

public class FComplex {
    private final float real;
    private final float imaginary;

    public FComplex(float real, float imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public FComplex(float real) {
        this(real, 0f);
    }

    public float getReal() {
        return real;
    }

    public float getImaginary() {
        return imaginary;
    }

    public FComplex conjugate(){
        return new FComplex(real, -imaginary);
    }

    public float abs(){
        if (Math.abs(real) < Math.abs(imaginary)) {
            if (imaginary == 0.0) {
                return Math.abs(real);
            }
            float q = real / imaginary;
            return (float) (Math.abs(imaginary) * Math.sqrt(1 + q * q));
        } else {
            if (real == 0.0) {
                return Math.abs(imaginary);
            }
            float q = imaginary / real;
            return (float) (Math.abs(real) * Math.sqrt(1 + q * q));
        }
    }
}
