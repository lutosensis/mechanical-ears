package be.wantho.data;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public interface FingerPrintStore {
    Document getDocument(int docid);
    int[][] get(int hash);
    char[] getRaw(int hash);
    int size();
}
