package be.wantho;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import be.wantho.data.FingerPrintStore;
import be.wantho.data.Match;

/**
 * Created by Wannes2 on 28/11/2017.
 */

public class Matcher {
    private final FingerPrintStore db;

    public Matcher(FingerPrintStore db) {
        this.db = db;
    }

    public List<Match> match(int[][] hashes){
        // sort on hash value to improve db cache hit rate
        List<int[]> hashes_sorted = Arrays.asList(hashes);
        Collections.sort(hashes_sorted, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[1] - o2[1];
            }
        });

        Map<String, Integer> matches = new HashMap<>();
        Map<Integer, Integer> matches_simple = new HashMap<>();

        for (int[] time_hash_pair : hashes) {
            int time = time_hash_pair[0], hash = time_hash_pair[1];
            int[][] time_doc_pairs = db.get(hash);
            for (int[] time_doc_pair : time_doc_pairs) {
                int time_match = time_doc_pair[0], docid = time_doc_pair[1];
                int offset = time - time_match;

                String key = docid + "," + offset;
                if(!matches.containsKey(key)){
                    matches.put(key, 0);
                }
                matches.put(key, matches.get(key) + 1);

                if(!matches_simple.containsKey(docid)){
                    matches_simple.put(docid, 0);
                }
                matches_simple.put(docid, matches_simple.get(docid) + 1);
            }
        }

        List<Match> out = new ArrayList<>();
//        for (Integer docid : matches_simple.keySet()) {
//            out.add(new Match(db.getDocument(docid), matches_simple.get(docid)));
//        }

        for (String key : matches.keySet()) {
            int docid = Integer.parseInt(key.split(",")[0]);
            out.add(new Match(db.getDocument(docid), matches.get(key)));
        }

        Collections.sort(out, new Comparator<Match>() {
            @Override
            public int compare(Match o1, Match o2) {
                return o2.count - o1.count;
            }
        });

        Set<Integer> seen = new HashSet<>();
        List<Match> filtered = new ArrayList<>();

        for (Match match : out) {
            if(!seen.contains(match.document.id)){
                seen.add(match.document.id);
                filtered.add(match);
            }
        }

        return filtered;
    }
}
