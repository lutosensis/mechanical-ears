package be.wantho.fft;

import org.apache.commons.math3.complex.Complex;
import org.jtransforms.fft.DoubleFFT_1D;
import org.jtransforms.fft.FloatFFT_1D;

import be.wantho.data.FComplex;

/**
 * Created by Wannes2 on 24/11/2017.
 */

public class STFT {
    public static void fft1D(FComplex[] signal){
        int n = signal.length;

        float[] coeff = new float[2*n];
        int i = 0;
        for(FComplex c:signal){
            coeff[i++] = c.getReal();
            coeff[i++] = c.getImaginary();
        }

        FloatFFT_1D fft = new FloatFFT_1D(n);
        fft.complexForward(coeff);

        for(i = 0; i < 2*n; i+=2){
            FComplex c    = new FComplex(coeff[i], coeff[i+1]);
            signal[i/2] = c;
        }
    }

    public static float[] pad(float[] y, int width, String mode){
        switch (mode){
            case "reflect":
            {
                float[] out = new float[y.length + 2*width];
                for(int i = 0; i < out.length; i++){
                    int j = Math.abs(i - width);
                    if(j >= y.length){
                        j = y.length - (j - y.length + 2);
                    }
                    out[i] = y[j];
                }
                return out;
            }
            default:
                throw new RuntimeException("unsupported padding mode: " + mode);
        }
    }

    public static float[][] frame(float[] y, int frame_length, int hop_length){
        // y_frames[i, j] == y[j * hop_length + i]

        int n_frames = 1 + ((y.length - frame_length) / hop_length);

        float[][] y_frames = new float[frame_length][n_frames];
        for(int i = 0; i < frame_length; i++){
            for(int j = 0; j < n_frames; j++){
                y_frames[i][j] = y[j * hop_length + i];
            }
        }

        return y_frames;
    }

    public static float[][] stft(float[] y, int n_fft, Integer hop_length, Integer win_length, float[] window,
                           boolean center, String pad_mode){
        if(win_length == null) win_length = n_fft;
        if(hop_length == null) hop_length = win_length / 4;

        if(center){
            y = pad(y, n_fft/2, pad_mode);
        }

        float[][] y_frames = frame(y, n_fft, hop_length);
        float[][] stft_matrix = new float[1 + n_fft / 2][y_frames[0].length];

        for(int column = 0; column < y_frames[0].length; column++) {
            for (int i = 0; i < y_frames.length; i++) {
                y_frames[i][column] *= (float) window[i];
            }
        }

        for(int column = 0; column < stft_matrix[0].length; column++){
            FComplex[] slice = new FComplex[y_frames.length];
            for(int i = 0; i < slice.length; i++){
                slice[i] = new FComplex(y_frames[i][column]);
            }

            fft1D(slice);

//            System.out.println("iter result");
//            for(int i = 0; i < 20; i++) System.out.print(slice[i] + ", ");
//            System.out.println("");

            for(int i = 0; i < stft_matrix.length; i++){
                stft_matrix[i][column] = slice[i].conjugate().abs();
            }
        }

        return stft_matrix;
    }
}
