package be.wantho.utils;

import java.util.Arrays;

/**
 * Created by Wannes2 on 25/11/2017.
 */

public class Numpy {

    public static float[][] nonzero(float[][] data){
        final float[][] result = new float[data.length][data[0].length];

        forEach(data, new DoubleCallback() {
            @Override
            public void consume(int y, int x, float data) {
                result[y][x] = data > 0 ? 1 : 0;
            }
        });

        return result;
    }

    public static float sum(float[][] data){
        final float[] sum = new float[1];
        forEach(data, new DoubleCallback() {
            @Override
            public void consume(int y, int x, float data) {
                sum[0] += data;
            }
        });
        return sum[0];
    }

    public static float[] column(float[][] data, int index){
        float[] col = new float[data.length];
        for(int y = 0; y < data.length; y++) col[y] = data[y][index];
        return col;
    }

    public static float mean(float[][] data){
        final float[] total = new float[]{0};
        forEach(data, new DoubleCallback() {
            @Override
            public void consume(int y, int x, float data) {
                total[0] += data;
            }
        });
        return total[0] / (data.length * data[0].length);
    }

    public static float max(float[][] data){
        final float[] max = new float[]{Float.NEGATIVE_INFINITY};
        forEach(data, new DoubleCallback() {
            @Override
            public void consume(int y, int x, float data) {
                max[0] = Math.max(max[0], data);
            }
        });
        return max[0];
    }

    public static void forEach(float[][] data, DoubleCallback callback){
        for(int y = 0; y < data.length; y++){
            for(int x = 0; x < data[0].length; x++){
                callback.consume(y, x, data[y][x]);
            }
        }
    }

    public static void map(float[][] data, DoubleMapper callback){
        for(int y = 0; y < data.length; y++){
            for(int x = 0; x < data[0].length; x++){
                data[y][x] = callback.map(y, x, data[y][x]);
            }
        }
    }

    public static boolean[] locmax(float[] vector){
        boolean[] mask = new boolean[vector.length];
        mask[0] = vector[0] > vector[1];
        mask[mask.length-1] = vector[vector.length-1] > vector[vector.length-2];

        for(int x = 1; x < mask.length-1; x++){
            mask[x] = vector[x] > vector[x-1] && vector[x] > vector[x+1];
        }

        return mask;
    }

    public static int[] locmaxIndex(float[] vector) {
        boolean[] mask = locmax(vector);
        int[] indices = new int[mask.length];
        int index_count = 0;
        for(int x = 0; x < mask.length; x++){
            if(mask[x]){
                indices[index_count] = x;
                index_count++;
            }
        }
        return Arrays.copyOfRange(indices, 0, index_count);
    }

    public static boolean[] greater_equal(float[] first, float[] second) {
        assert second.length == first.length;

        int l = first.length;
        boolean[] result = new boolean[l];

        for(int x = 0; x < l; x++) result[x] = first[x] >= first[x];
        return result;
    }

    public interface DoubleCallback {
        void consume(int y, int x, float data);
    }

    public interface DoubleMapper {
        float map(int y, int x, float data);
    }
}
