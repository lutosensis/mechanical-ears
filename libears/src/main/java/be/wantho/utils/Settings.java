package be.wantho.utils;

/**
 * Created by Wannes2 on 27/11/2017.
 */

public class Settings {
    public static final int BUCKET_SIZE_WIDTH = 7; // bucket size of 128
    public static final int DOC_ID_MAX_WIDTH = 14; // max of 16k docids
    public static final int DOC_ID_COMPACT_WIDTH = DOC_ID_MAX_WIDTH - BUCKET_SIZE_WIDTH;
    public static final int BUCKET_SIZE = (int) Math.pow(2, BUCKET_SIZE_WIDTH);
    public static final int HASH_WIDTH = 20;
}
