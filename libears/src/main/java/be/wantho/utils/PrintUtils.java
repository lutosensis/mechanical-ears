package be.wantho.utils;

/**
 * Created by Wannes2 on 25/11/2017.
 */

public class PrintUtils {
    public static void prettyPrintArray(float[][] array, int showLimit){
        for(int y = 0; y < array.length; y++){
            if(y >= showLimit && y < array.length - showLimit) continue;
            System.out.print("[ ");
            for(int x = 0; x < array[0].length; x++){
                if(x >= showLimit && x < array[0].length - showLimit) continue;
                System.out.print(String.format("%9.9E ,", array[y][x]));
                if(x == showLimit-1) System.out.print(" ... ");
            }
            System.out.println(" ]");
            if(y == showLimit-1){
                System.out.println("\n.\n.\n.\n");
            }
        }
    }
}
