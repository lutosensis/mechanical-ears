package be.wantho.utils;

import static be.wantho.utils.Settings.BUCKET_SIZE_WIDTH;
import static be.wantho.utils.Settings.DOC_ID_COMPACT_WIDTH;

/**
 * Created by Wannes2 on 27/11/2017.
 */

public class FingerprintPacker {

    public static int[] unpack(int packed, int index){
        int doc_id = ((packed & ((1 << DOC_ID_COMPACT_WIDTH) - 1)) << BUCKET_SIZE_WIDTH) | index;
        int time = packed >> DOC_ID_COMPACT_WIDTH;
        return new int[]{time, doc_id};
    }

    public static char pack(int time, int doc_id){
        int docid_compact = doc_id >> BUCKET_SIZE_WIDTH;
        return (char)((time << DOC_ID_COMPACT_WIDTH) | docid_compact);
    }
}
