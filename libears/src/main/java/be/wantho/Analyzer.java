package be.wantho;

import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import be.tarsos.dsp.util.fft.HannWindow;
import be.wantho.fft.STFT;
import be.wantho.utils.Numpy;
import be.wantho.utils.PrintUtils;
import biz.source_code.dsp.filter.IirFilter;
import biz.source_code.dsp.filter.IirFilterCoefficients;

/**
 * Created by Wannes2 on 24/11/2017.
 */

public class Analyzer {
    public static class Builder {
        private float density;
        private int target_sr;
        private int n_fft;
        private int n_hop;
        private int shifts;
        private float f_sd;
        private int maxpksperframe;
        private int maxpairsperpeak;
        private int targetdf;
        private int mindt;
        private int targetdt;
        private float soundfiledur;
        private float soundfiletotaldur;
        private int soundfilecount;
        private boolean fail_on_error;

        public Builder() {
            Analyzer stub = new Analyzer();
            // set default values
            density = stub.density;
            target_sr = stub.target_sr;
            n_fft = stub.n_fft;
            n_hop = stub.n_hop;
            shifts = stub.shifts;
            f_sd = stub.f_sd;
            maxpksperframe = stub.maxpksperframe;
            maxpairsperpeak = stub.maxpairsperpeak;
            targetdf = stub.targetdf;
            mindt = stub.mindt;
            targetdt = stub.targetdt;
            soundfiledur = stub.soundfiledur;
            soundfiletotaldur = stub.soundfiletotaldur;
            soundfilecount = stub.soundfilecount;
            fail_on_error = stub.fail_on_error;
        }

        public static Builder with(){
            return new Builder();
        }

        public Builder density(float density) {
            this.density = density;
            return this;
        }

        public Builder target_sr(int target_sr) {
            this.target_sr = target_sr;
            return this;
        }

        public Builder n_fft(int n_fft) {
            this.n_fft = n_fft;
            return this;
        }

        public Builder n_hop(int n_hop) {
            this.n_hop = n_hop;
            return this;
        }

        public Builder shifts(int shifts) {
            this.shifts = shifts;
            return this;
        }

        public Builder f_sd(float f_sd) {
            this.f_sd = f_sd;
            return this;
        }

        public Builder maxpksperframe(int maxpksperframe) {
            this.maxpksperframe = maxpksperframe;
            return this;
        }

        public Builder maxpairsperpeak(int maxpairsperpeak) {
            this.maxpairsperpeak = maxpairsperpeak;
            return this;
        }

        public Builder targetdf(int targetdf) {
            this.targetdf = targetdf;
            return this;
        }

        public Builder mindt(int mindt) {
            this.mindt = mindt;
            return this;
        }

        public Builder targetdt(int targetdt) {
            this.targetdt = targetdt;
            return this;
        }

        public Builder soundfiledur(float soundfiledur) {
            this.soundfiledur = soundfiledur;
            return this;
        }

        public Builder soundfiletotaldur(float soundfiletotaldur) {
            this.soundfiletotaldur = soundfiletotaldur;
            return this;
        }

        public Builder soundfilecount(int soundfilecount) {
            this.soundfilecount = soundfilecount;
            return this;
        }

        public Builder fail_on_error(boolean fail_on_error) {
            this.fail_on_error = fail_on_error;
            return this;
        }

        public Analyzer build(){
            return new Analyzer(
                density, target_sr, n_fft, n_hop, shifts, f_sd, maxpksperframe, maxpairsperpeak,
                targetdf, mindt, targetdt, soundfiledur, soundfiletotaldur, soundfilecount, fail_on_error
            );
        }
    }

    private static final boolean DEBUG = false;

    public static final float DENSITY = 20.0f;
    // OVERSAMP > 1 tries to generate extra landmarks by decaying faster
    public static final int OVERSAMP = 1;
    // 512 pt FFT @ 11025 Hz, 50% hop
    // t_win = 0.0464
    // t_hop = 0.0232
    // Just specify n_fft
    public static final int N_FFT = 512;
    public static final int N_HOP = 256;
    // spectrogram enhancement
    public static final float HPF_POLE = 0.98f;
    // Globals defining packing of landmarks into hashes
    public static final int F1_BITS = 8;
    public static final int DF_BITS = 6;
    public static final int DT_BITS = 6;
    // derived constants
    public static final int B1_MASK = (1 << F1_BITS) - 1;
    public static final int B1_SHIFT = DF_BITS + DT_BITS;
    public static final int DF_MASK = (1 << DF_BITS) - 1;
    public static final int DF_SHIFT = DT_BITS;
    public static final int DT_MASK = (1 << DT_BITS) - 1;

    public static int[][] landmarks2hashes(List<Landmark> landmarks){
        int[][] hashes = new int[landmarks.size()][2];
        // copy all time (col) values from the landmarks to the first col
        // construct the hash from the remaining landmark attrs
        for(int i = 0; i < landmarks.size(); i++){
            Landmark l = landmarks.get(i);
            hashes[i][0] = l.col;
            hashes[i][1] = (
                    ((l.peak & B1_MASK) << B1_SHIFT)
                  | (((l.peak2 - l.peak) & DF_MASK) << DF_SHIFT)
                  | (l.col_difference & DT_MASK)
            );
        }

        return hashes;
    }

    private final float density;
    private final int target_sr;
    private final int n_fft;
    private final int n_hop;
    private final int shifts;
    private final float f_sd;
    private final int maxpksperframe;
    private final int maxpairsperpeak;
    private final int targetdf;
    private final int mindt;
    private final int targetdt;
    private final float soundfiledur;
    private final float soundfiletotaldur;
    private final int soundfilecount;
    private final boolean fail_on_error;

    private float   __sp_width = -1;
    private int      __sp_len   = -1;
    private float[] __sp_vals  = null;

    public Analyzer() {
        this.density = DENSITY;
        this.target_sr = 11025;
        this.n_fft = N_FFT;
        this.n_hop = N_HOP;
        this.shifts = 1;
        this.f_sd = 30.0f;
        this.maxpksperframe = 5;
        this.maxpairsperpeak = 3;
        this.targetdf = 31;
        this.mindt = 2;
        this.targetdt = 63;
        this.soundfiledur = 0.0f;
        this.soundfiletotaldur = 0.0f;
        this.soundfilecount = 0;
        this.fail_on_error = true;
    }

    private Analyzer(float density, int target_sr, int n_fft, int n_hop, int shifts, float f_sd,
                     int maxpksperframe, int maxpairsperpeak, int targetdf, int mindt, int targetdt,
                     float soundfiledur, float soundfiletotaldur, int soundfilecount, boolean fail_on_error) {
        this.density = density;
        this.target_sr = target_sr;
        this.n_fft = n_fft;
        this.n_hop = n_hop;
        this.shifts = shifts;
        this.f_sd = f_sd;
        this.maxpksperframe = maxpksperframe;
        this.maxpairsperpeak = maxpairsperpeak;
        this.targetdf = targetdf;
        this.mindt = mindt;
        this.targetdt = targetdt;
        this.soundfiledur = soundfiledur;
        this.soundfiletotaldur = soundfiletotaldur;
        this.soundfilecount = soundfilecount;
        this.fail_on_error = fail_on_error;
    }

    private float[] spreadpeaksinvector(float[] vector) {
        return spreadpeaksinvector(vector, 4.0f);
    }

    private float[] spreadpeaksinvector(float[] vector, float width) {
        int npts = vector.length;
        int[] peaks = Numpy.locmaxIndex(vector);
        float[] vector_filtered = new float[peaks.length];

        for(int i = 0; i < vector_filtered.length; i++) vector_filtered[i] = vector[peaks[i]];

        if(DEBUG)
            System.out.println("peaks " + peaks.length);

        return spreadpeaks(peaks, vector_filtered, npts, width, null);
    }

    private float[] spreadpeaks(int[] index, float[] values, Integer npoints, float width, float[] base){
        assert index.length == values.length;

        float[] vec;
        if(base == null) {
            vec = new float[npoints];
        }else{
            npoints = base.length;
            vec = base.clone();
        }

        // Cached values between identical spreadpeaks calculations
        if(width != __sp_width || npoints != __sp_len){
            __sp_width = width;
            __sp_len = npoints;
            __sp_vals = new float[npoints*2+1];

            int sp_index = 0;
            for(int i = -npoints; i < npoints+1; i++){
                __sp_vals[sp_index] = (float) Math.exp(-.5 * Math.pow(i / (float)width, 2));
                sp_index++;
            }
        }

        for(int i = 0; i < index.length; i++){
            int pos = index[i];
            float val = values[i];

            for(int j = 0; j < npoints; j++){
                int sp_index = j + npoints - pos;
                vec[j] = Math.max(vec[j], __sp_vals[sp_index] * val);
            }
        }

        return vec;
    }

    private float[][] decaying_threshold_fwd_prune(float[][] sgram, float a_dec){
        int srows = sgram.length;
        int scols = sgram[0].length;

        // find the max row value for each row
        int scol_limit = Math.min(scols, 10);
        float[] max_values = new float[srows];
        Arrays.fill(max_values, Float.NEGATIVE_INFINITY);

        for(int y = 0; y < srows; y++)
        for(int x = 0; x < scol_limit; x++)
            max_values[y] = Math.max(max_values[y], sgram[y][x]);

        float[] stresh = spreadpeaksinvector(max_values, f_sd);
        float[][] peaks = new float[srows][scols];

        int __sp_pts = stresh.length;
        float[] __sp_v = __sp_vals;

        for(int col = 0; col < scols; col++){
            final float[] s_col = new float[srows];
            for(int y = 0; y < srows; y++){
                s_col[y] = sgram[y][col];
            }

            // Find local magnitude peaks that are above threshold
            boolean[] col_localmax = Numpy.locmax(s_col);
            List<Integer> maxAndAboveTresh = new ArrayList<>();
            for(int y = 0; y < srows; y++){
                if(col_localmax[y] && s_col[y] > stresh[y]){
                    maxAndAboveTresh.add(y);
                }
            }

            // Sort indices in descending order by value inside s_col
            Collections.sort(maxAndAboveTresh, new Comparator<Integer>() {
                @Override
                public int compare(Integer index1, Integer index2) {
                    float compare = s_col[index2] - s_col[index1];
                    if(compare == 0.0) return 0;
                    else return compare > 0 ? 1 : -1;
                }
            });

            for(int y = 0; y < Math.min(maxAndAboveTresh.size(), maxpksperframe); y++){
                int peakpos = maxAndAboveTresh.get(y);
                float val = s_col[peakpos];

                for(int i = 0; i < stresh.length; i++){
                    stresh[i] = Math.max(stresh[i], val * __sp_v[__sp_pts - peakpos + i]);
                }

                peaks[peakpos][col] = 1;
            }

            // update threshold values with a_dec
            for(int i = 0; i < stresh.length; i++) stresh[i] *= a_dec;
        }
        return peaks;
    }

    private float[][] decaying_threshold_bwd_prune_peaks(float[][] sgram, float[][] peaks, float a_dec){
        int scols = sgram[0].length;

        // get the last column of sgram
        float[] lastcol = Numpy.column(sgram, scols-1);

        // backwards filter to prune peaks
        float[] stresh = spreadpeaksinvector(lastcol, f_sd);

        for(int col = scols; col > 0; col--){
            float[] prevCol = Numpy.column(peaks, col-1);
            final float[] prevColSgram = Numpy.column(sgram, col-1);

            List<Integer> pkposs = new ArrayList<>();
            for(int i = 0; i < prevCol.length; i++){
                if(prevCol[i] > 0) pkposs.add(i);
            }

            // Sort pkposs indices by the sgram values in descending order
            Collections.sort(pkposs, new Comparator<Integer>() {
                @Override
                public int compare(Integer index1, Integer index2) {
                    float compare = prevColSgram[index2] - prevColSgram[index1];
                    if(compare == 0.0) return 0;
                    else return compare > 0 ? 1 : -1;
                }
            });

            for(int i = 0; i < pkposs.size(); i++){
                int peakpos = pkposs.get(i);
                float val = prevColSgram[peakpos];
                if(val >= stresh[peakpos]){
                    // re-calc threshold
                    stresh = spreadpeaks(new int[]{peakpos}, new float[]{val}, null, f_sd, stresh);
                    // Delete any following peak (threshold should, but be sure)
                    if(col < scols)
                        peaks[peakpos][col] = 0;
                }else{
                    // prune (remove) the peak
                    peaks[peakpos][col-1] = 0;
                }
            }

            for(int i = 0; i < stresh.length; i++) stresh[i] *= a_dec;
        }
        return peaks;
    }

    public List<Pair<Integer, Integer>> find_peaks(float[] d, int sr) {
        if(d.length == 0) return new ArrayList<>();

        // masking envelope decay constant
        float a_dec = (float) Math.pow(1.0 - 0.01*(density*Math.sqrt(n_hop/352.8)/35.0), 1.0/OVERSAMP);
        // take spectogram
        if(DEBUG)
            System.out.println(a_dec);

        float[] window_curve = new HannWindow().generateCurve(n_fft+2);
        float[] window_sliced = Arrays.copyOfRange(window_curve, 1, window_curve.length-1);

        float[][] sgram = STFT.stft(d, n_fft, n_hop, null, window_sliced, true, "reflect");

        if(DEBUG)
            PrintUtils.prettyPrintArray(sgram, 3);

        // Fetch the max value from the spectogram
        float sgrammax = Float.NEGATIVE_INFINITY;
        for(int y = 0; y < sgram.length; y++)
        for(int x = 0; x < sgram[0].length; x++)
            sgrammax = Math.max(sgrammax, sgram[y][x]);

        float sgramsum = 0;

        // Set a lower cutoff at sgrammax/1e6 and take the logarithm of each value
        // Also keeps a sum of all resulting values to later on calculate the average
        for(int y = 0; y < sgram.length; y++){
            for(int x = 0; x < sgram[0].length; x++){
                sgram[y][x] = (float) Math.log(Math.max(sgrammax/1e6, sgram[y][x]));
                sgramsum += sgram[y][x];
            }
        }

        // Subtract the average of the spectrum from each value
        float sgrammean = sgramsum / (sgram.length * sgram[0].length);

        for(int y = 0; y < sgram.length; y++)
        for(int x = 0; x < sgram[0].length; x++)
            sgram[y][x] -= sgrammean;

        if(DEBUG){
            System.out.println("max " + sgrammax);
            System.out.println("sgram subtracted mean");
            PrintUtils.prettyPrintArray(sgram, 3);
        }

        // filter the spectogram signal using a IIR filter
        // ported from scipy.signal.lfilter, original code filters
        // each row in the spectogram in a linear fashion from left->right
        float[][] sgram_filtered = new float[sgram.length-1][sgram[0].length];

        for(int y = 0; y < sgram.length-1; y++){
            IirFilterCoefficients coofs = new IirFilterCoefficients();
            coofs.b = new double[]{1, -1};
            coofs.a = new double[]{1, -Math.pow(HPF_POLE, 1/OVERSAMP)};
            IirFilter lfilter = new IirFilter(coofs);

            for(int x = 0; x < sgram[0].length; x++){
                sgram_filtered[y][x] = (float) lfilter.step(sgram[y][x]);
            }
        }

        sgram = sgram_filtered;

        if(DEBUG){
            System.out.println("lfiltered");
            PrintUtils.prettyPrintArray(sgram, 10);
        }

        float[][] peaks = decaying_threshold_fwd_prune(sgram, a_dec);

        if(DEBUG){
            System.out.println("peaks 1");
            PrintUtils.prettyPrintArray(peaks, 3);
            System.out.println("nonzeros " + Numpy.sum(Numpy.nonzero(peaks)));
        }

        peaks = decaying_threshold_bwd_prune_peaks(sgram, peaks, a_dec);

        if(DEBUG)
            System.out.println("nonzeros bwd " + Numpy.sum(Numpy.nonzero(peaks)));

        float[][] peaksNonzero = Numpy.nonzero(peaks);

        int scols = sgram[0].length;
        List<Pair<Integer, Integer>> pklist = new ArrayList<>();
        for(int col = 0; col < scols; col++){
            float[] colvals = Numpy.column(peaksNonzero, col);
            for(int x = 0; x < colvals.length; x++){
                if(colvals[x] > 0){
                    pklist.add(new Pair<>(col, x));
                }
            }
        }
        return pklist;
    }

    public static class Landmark {
        public int col;
        public int peak;
        public int peak2;
        public int col_difference;

        public Landmark(int col, int peak, int peak2, int col_difference) {
            this.col = col;
            this.peak = peak;
            this.peak2 = peak2;
            this.col_difference = col_difference;
        }
    }

    private List<Landmark> peaks2landmarks(List<Pair<Integer, Integer>> pklist){
        List<Landmark> landmarks = new ArrayList<>();
        if(pklist.size() > 0){
            // column of last peak (+ 1)
            int scols = pklist.get(pklist.size() - 1).getFirst() + 1;
            List<List<Integer>> peaks_at = new ArrayList<>();
            for(int i = 0; i < scols; i++) peaks_at.add(new ArrayList<Integer>());

            for (Pair<Integer, Integer> col_bin : pklist) {
                peaks_at.get(col_bin.getFirst()).add(col_bin.getSecond());
            }

            // create the landmarks
            for(int col = 0; col < scols; col++){
                for (Integer peak : peaks_at.get(col)) {
                    int pairsthispeak = 0;
                    for(int col2 = col + mindt; col2 < Math.min(scols, col + targetdt); col2++){
                        if(pairsthispeak < maxpairsperpeak){
                            for (Integer peak2 : peaks_at.get(col2)) {
                                if(Math.abs(peak2 - peak) < targetdf && pairsthispeak < maxpairsperpeak){
                                    landmarks.add(new Landmark(col, peak, peak2, col2-col));
                                    pairsthispeak++;
                                }
                            }
                        }
                    }
                }
            }
        }

        return landmarks;
    }

    public List<List<Pair<Integer, Integer>>> pcm2peaks(float[] d, Integer shifts){
        int sr = target_sr;
        List<List<Pair<Integer, Integer>>> peaks = new ArrayList<>();

        if(shifts == null || shifts < 2){
            peaks.add(find_peaks(d, sr));
        }else{
            for (int i = 0; i < shifts; i++) {
                int shiftamps = (int)(((float)i)/shifts*n_hop);
                peaks.add(find_peaks(Arrays.copyOfRange(d, shiftamps, d.length), sr));
            }
        }

        return peaks;
    }

    public int[][] pcm2hashes(float[] d){
        List<List<Pair<Integer, Integer>>> peaks = pcm2peaks(d, shifts);

        if(peaks.size() == 0) return new int[0][0];

        Map<Long, int[]> dups = new HashMap<>();

        for (List<Pair<Integer, Integer>> singlepeaklist : peaks) {
            int[][] hashes = landmarks2hashes(peaks2landmarks(singlepeaklist));
            for(int y = 0; y < hashes.length; y++){
                int[] row = hashes[y];
                long packed = (((long)row[0]) << 32) | (row[1] & 0xffffffffL);
                dups.put(packed, row);
            }
        }

        int[][] hashescombined = new int[dups.size()][2];
        SortedSet<Long> sorted = new TreeSet<>(dups.keySet());
        int y = 0;
        for (Long packed : sorted) {
            hashescombined[y] = dups.get(packed);
            y++;
        }

        return hashescombined;
    }
}
